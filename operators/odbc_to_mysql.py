# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pandas as pd

from sqlalchemy import create_engine
from airflow.models import BaseOperator
from AirflowOdbcPlugin.hooks.odbc_hook import OdbcHook
from airflow.hooks.mysql_hook import MySqlHook
from airflow.utils.decorators import apply_defaults
from tempfile import NamedTemporaryFile


class OdbcToMysqlTransfer(BaseOperator):

    template_fields = ('sql', 'mysql_table', 'mysql_preoperator',
                       'mysql_postoperator')
    template_ext = ('.sql',)
    ui_color = '#a0e08c'

    @apply_defaults
    def __init__(
            self,
            sql,
            mysql_table,
            odbc_dsn='odbc_default',
            mysql_conn_id='mysql_default',
            mysql_preoperator=None,
            mysql_postoperator=None,
            mysql_database=None,
            bulk_load=False,
            mysql_table_index=None,
            *args, **kwargs):
        super(OdbcToMysqlTransfer, self).__init__(*args, **kwargs)
        self.sql = sql
        self.mysql_table = mysql_table
        self.mysql_table_index = mysql_table_index
        self.mysql_conn_id = mysql_conn_id
        self.mysql_preoperator = mysql_preoperator
        self.mysql_postoperator = mysql_postoperator
        self.odbc_dsn = odbc_dsn
        self.bulk_load = bulk_load
        self.mysql_database = mysql_database

    def __get_mysql_engine(self, config):
        """
        Return SQL Alchemy Database Engine

        :param config: Airflow context
        :return:
        """

        return create_engine(
            'mysql+{driver}://{username}:{password}@{host}:{port}/{schema}'.format(
                driver='pymysql',
                username=config.login,
                password=config.password or '',
                host=config.host or 'localhost',
                port=3306 if not config.port else int(config.port),
                schema=self.mysql_database,
            )
        )

    def execute(self, context):
        """
        Dump SQL query to pandas pickle and load the dataframe to mysql database using SQL Alchemy
        :param context: Airflow context
        """
        odbc = OdbcHook(dsn=self.odbc_dsn)
        tmpfile = NamedTemporaryFile()

        self.log.info('Fetching results from query `{}`'.format(self.sql))
        odbc.to_pickle(sql=self.sql, pickle_filepath=tmpfile.name, fetch_size=500)

        mysql = MySqlHook(mysql_conn_id=self.mysql_conn_id, schema=self.mysql_database)
        engine = self.__get_mysql_engine(mysql.get_connection(self.mysql_conn_id))

        if self.mysql_preoperator:
            mysql.run(self.mysql_preoperator)

        self.log.info('Inserting rows into Mysql')
        df = pd.read_pickle(tmpfile.name)
        df.to_sql(self.mysql_table, engine, if_exists='append', index=False, chunksize=1000)

        tmpfile.close()

        if self.mysql_postoperator:
            mysql.run(self.mysql_postoperator)

        self.log.info('Done.')
