# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from AirflowOdbcPlugin.hooks.odbc_hook import OdbcHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

class OdbcOperator(BaseOperator):

    @apply_defaults
    def __init__(self, sql, dsn, parameters=None, *args, **kwargs):
        super(OdbcOperator, self).__init__(*args, **kwargs)
        self.dsn = dsn
        self.sql = sql
        self.parameters = parameters

    def execute(self, context):
        hook = OdbcHook(dsn=self.dsn)

        hook.run(self.sql, parameters=self.parameters)