# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pyodbc
import pandas as pd

from contextlib import closing
from airflow.hooks.dbapi_hook import DbApiHook


class OdbcHook(DbApiHook):
    """
    Interact with Database via ODBC.
    """
    conn_name_attr = 'odbc_conn_attr'
    supports_autocommit = False

    def __init__(self, dsn, *args, **kwargs):
        super(OdbcHook, self).__init__(*args, **kwargs)
        self.dsn = dsn

    def get_conn(self):
        """
        Return ODBC connection object

        :return: connection object
        """
        conn = pyodbc.connect('DSN={}'.format(self.dsn))
        conn.setencoding(encoding='utf-8')
        conn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
        conn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
        print(self.dsn)
        return conn

    def to_pickle(self, sql: str, pickle_filepath: str, fetch_size: int):
        """
        Dump SQL to pandas pickle

        :param sql: SQL query script to run
        :param pickle_filepath: The path of pickle file
        :param fetch_size: Fetch Size for pandas dataframe
        """
        with closing(self.get_conn()) as con:
            result = pd.read_sql(sql=sql, con=con, chunksize=fetch_size)

            batch = 0
            total = 0

            df = pd.DataFrame()

            for chunk in result:

                if len(df) == 0:
                    df = chunk.copy()
                else:
                    df = df.append(chunk, ignore_index=True)
                batch += 1
                total += len(chunk)
                self.log.info('[{}] batch {}, {} row(s) downloaded'.format(pickle_filepath, batch, total))

            df.to_pickle(pickle_filepath)

    def bulk_dump(self, table, tmp_file):
        """
        Dumps a database table into a tab-delimited file

        :param table: The name of the source table
        :type table: str
        :param tmp_file: The path of the target file
        :type tmp_file: str
        """
        raise NotImplementedError()

    def bulk_load(self, table, tmp_file):
        """
        Loads a tab-delimited file into a database table

        :param table: The name of the target table
        :type table: str
        :param tmp_file: The path of the file to load into the table
        :type tmp_file: str
        """
        raise NotImplementedError()
